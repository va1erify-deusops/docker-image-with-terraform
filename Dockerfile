FROM ubuntu:20.04

#Configure tz-data
ENV TZ=Europe/Moscow
RUN ln -snf /usr/share/zoneinfo/$TZ /etc/localtime && echo $TZ

RUN apt update
RUN apt install curl unzip -y
RUN curl -L -o terraform.zip https://hashicorp-releases.yandexcloud.net/terraform/1.7.4/terraform_1.7.4_linux_amd64.zip 
RUN unzip terraform.zip  && \
    mv terraform /usr/bin/ && \
    rm terraform.zip && \
    rm -rf /var/lib/apt/lists/*

CMD ["/bin/bash"]
